
# Recommended future actions at ESS level to promote sharing code and use of open source software 
*LAST UPDATE: March 10, 2023*

 - Internships, possible staff exchange between NSIs to exchange and implement best practices for open source 
 - Peer review of code between NSIs
 - Promote certain tool(s) in the ESS for packaging and documenting OSS   
 - Specific ESTP courses dedicated to the creation of packages, more experienced NSIs could help less experienced ones to package their software
 - Provide appropriate training to familiarise with this new way of thinking and learn about best practices and benefits of using OSS for non OSS native workforce, including management executives
 - Form a group to continue the discussion on the above mentioned topics and organise a workshop
    + to exchange best practices
    + to define the requirements and conditions for possible code review   
    + to create security guidelines and minimum requirements for sharing code
    + to create a curated list of open source tools to be promoted for official statistics
    + to discuss how to maintain the common guidelines and curated list of recommended open source tools   
  

