# Background
*LAST UPDATE: March 10, 2023*

This repository contains the output of the work done by the group on Open Source for Official Statistics (OS4OS). 

The group was created as a follow up of previous discussions on open source in the Working Group Methododology on [April 28, 2021](https://ec.europa.eu/eurostat/cros/content/6th-meeting-28-april-2021-online_en) and [April 6, 2022](https://ec.europa.eu/eurostat/cros/content/7th-meeting-06-april-2022-online_en); the Task Force Trusted Smart Statistics on March 10, 2022 and the DIME-ITDG plenary on [June 29, 2022](https://ec.europa.eu/eurostat/cros/content/dime-itdg-plenary-29-30-june-2022_en). The group had the following mandate: 

> ... share and review current practices and lessons learnt on the use of Open Source Software (OSS) for statistical purposes, and look at what additional work can be carried out together in the ESS, including the governance and tasks at technical level.[^1]

[^1]: [Minutes of the DIME-ITDG plenary meeting June 29-30, 2022](https://ec.europa.eu/eurostat/cros/system/files/dime-itdg_plenarymeeting_29-30june2022_minutes_0.pdf)

After the call to nominate representatives to this group, from the following Members States and international organisations participated in the preparation of this document in protocol order:
 - Belgium
 - Denmark
 - Ireland
 - Greece
 - Spain
 - France
 - Croatia
 - Italy
 - Cyprus
 - Latvia
 - Netherlands
 - Austria
 - Poland
 - Finland 
 - Sweden
 - Iceland
 - Norway
 - Lichtenstein
 - OECD
 
The work is divided into 4 main topics:

- [Principles](principles.md)
- [Bottlenecks](bottlenecks.md)
- [Best Practices](best_practices.md)
- [Recommended future actions](actions.md)





