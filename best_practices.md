# Best practices and enablers
*LAST UPDATE: June 9, 2023*

## 1. Institutional environment

### Legal environment 

 - In France since 2016 source codes developed by French administrations must be open sourced (Open Government)
   
     <details><summary> There are some exceptions </summary>
     - Intellectual property rights held by third parties <br>
     - Security of information systems <br>
     - Secrets protected by the law (incl. statistical confidentiality)
     </details>

     – Access to source code is ensured by online publication under an open source licence (right to use, modify and share)<br>
     – Source code publishing is allowed even in the absence of a formal request for access<br>
     – For new projects, a few open source licenses can be used
 - [EC Communication (2020)](https://ec.europa.eu/info/sites/default/files/en_ec_open_source_strategy_2020-2023.pdf) to create a working culture based on the principles of open source
 - [EC Decision (2021)](https://ec.europa.eu/transparency/documents-register/detail?ref=C(2021)8759&lang=en) enabling Commission services may make its software available for reuse under an open source licence
 - The general ongoing trend in public administrations mentioned above also helps with interoperability of different solutions throughout in the EU and also in the NSIs by making NSI employees more versatile and mobile - across various domains within an NSI (e.g. in terms of technical coding skills and using the same OSS) but also from/to other agencies of national administration. This development makes the public administration more attractive for the research institutes and market sector to cooperate with. 
 

### Organisational environment

- In order to encourage the adoption of an open-source culture in an organization, it can be useful to identify a specific structure inside the organization that is responsible for putting forward this topic. Such structures are generally named Open Source Program Offices (OSPO). The range of missions covered by an OSPO can be quite diverse, but can be summarised in a few large topics:
    - advise the organization senior management to elaborate an open-source strategy. In particular, the OSPO is responsible for pushing a set of guidelines needed to open code safely (see next section)
    - administration of fundamentals open-source services (open-source platforms such as GitHub and GitLab, availability of CI/CD tools for open-source projects, etc.)
    - spread the open-source culture among the organization (documentation of the open-source framework and guidelines, training programs, etc.)
    - participate in open-source communities from the public sector
- Either if someone chose a commercial or open source product, they have to be sure that the used software prudcuts fulfill the operational parameters of the given organisation. This is not an inherent limitation to use open source tools, but it is particular to the organizational needs and structure. 

## 2. Guidelines

- The UK public sector has guidelines for using open source software for [reproducible pipelines](https://analysisfunction.civilservice.gov.uk/support/reproducible-analytical-pipelines/), see also the [Q2022 presentation from ONS UK](https://lsvdv-my.sharepoint.com/:p:/g/personal/ingabal_stat_gov_lt/EdmSvkvsuTdFs3lin8SYyyABgiMGWGbDQVE4TEPTqqcx8w?rtime=tZ4jZdTH2kg).

- The Statistical Information systems Collaboration Community ([SIS-CC](https://siscc.org/)) led by the Statistics and Data Directorate of [OECD](https://www.oecd.org/sdd/) has a produced a checklist for open source projects:

```{dropdown}
    -  Project has a basic landing page to explain what it is about, who the owner is, and links to additional information i.e. How to contribute<br>
    -  Project has a LICENCE file which is applied to each component along with the copyright notice<br>
    -  Project has basic documentation (README, CONTRIBUTING, CODE_OF_CONDUCT, development guidelines)<br>
    -  Each repository is suitable named e.g. #-#-#<br>
    -  Naming convention is followed for each repository<br>
    -  Provide details to explain how to report issues<br>
    -  An issue queue is exposed at the appropriate level which is up-to-date, with issues clearly organised and labelled<br>
    -  The code is clearly commented, documenting intentions and edge cases<br>
    -  Project uses consistent code conventions and clear function/method/variable names<br>
    -  There are no sensitive materials in the revision history, issues, or pull requests (for example, passwords or other non-public information)<br>
    -  Project has a product roadmap exposing the overall project vision and directions<br>
    -  Agree who will manage interactions (responding to issues, reviewing and merging pull requests)<br>
    -  Prepare a communications plan for launching and promoting the project<br>
    -  A clear outline of the CI/CD process to allow the project to progress efficiently<br>
    -  Agreed on the template for format for release notes<br>
    -  Provide details of how to install software<br>
    -  Expose a high-level technical architecture<br>
    -  Clear list of contributors at component level<br>
    -  Maintainers are aware of code of conduct and their responsibilities<br>
    -  Create a 'Beginners guide' i.e. How to get started
```

- At Insee (France), the open source program office (OSPO) has developed a process to open the code of a given project. It involves a set of requirements based on the "good - better - best" principle, in order to promote best practices while at the same time avoiding discouraging statisticians to open their code by being too rigorous.

```{dropdown}
- [Mandatory] Code relevancy. One-shot, undocumented or outdated code should not be distributed.
- [Mandatory] Use of an open-source license. A 2016 law specifies the list of [admissible licenses](https://inseefr.gitlab.io/open-source/site-documentaire/ouvrir-code/choixLicence/). The chosen license must be compliant with the licenses of the components that may be used in the project.
- [Mandatory] Use of Git. The use of version control through git is mandatory. The lifecycle of the project must be apparent.
- [Mandatory] Documentation. The code must be documented, and a `README` file should describe the context of the project, what it does, how to use it and how to contribute if relevant.
- [Mandatory] Security. The code should not include sensitive data. The code and its dependencies should also not contain critical vulnerabilities.
- [Recommended] Portability. In order to be reused, the code should be portable. For instance, it should not include absolute paths. It should also be minimally adherent to specific operating systems.
- [Ideal] Contribution guidelines, described in a `CONTRIBUTING.md` file.
- [Ideal] Use of English in the code and to document the project.
- [Ideal] Quality. An open-source project should adhere to coding best practices (code quality, project structure, etc.) so as to be readable and maintainable.
```

When these requirements are met, the OSPO will generally organize a peer review by a voluntary reviewer. Finally, a meeting is organized with the hierarchy of the contributing agent or team to determine responsibilities and maintenance constraints over time.

## 3. Catalogues of OSS software and tools
 - In France there is a central place to store and share open source code: https://code.gouv.fr/
 - In France also common catalogues are maintained for the OSS tools in the public sector: https://catalogue.numerique.gouv.fr/ and https://sill.etalab.gouv.fr/software
 - The EC created a central place for the code developed  for open source projects shared
by the institutions of the European Union (bodies, offices and agencies): https://code.europa.eu/info/about 
 - Statistics Netherlands maintains a list of open source tools and software for official statistics: https://github.com/SNStatComp/awesome-official-statistics-software

## 4. Technical skills improvement, knowledge exchange
 - Trainings
     - The Linux foundation has a free [training for beginners on OSS development](https://training.linuxfoundation.org/training/beginners-guide-open-source-software-development/)
     - Eurostat provides training through the [ESTP program](https://ec.europa.eu/eurostat/cros/content/estp-training-offer_en) for open source software (R, Python) in official statistics
 - Hackathons
     - The Open Source Programme Office (OSPO) of the Commission orginises [bug-hunting hackathons](https://joinup.ec.europa.eu/collection/ec-ospo)
     - Eurostat organises [the EU Big Data hackathons](https://ec.europa.eu/eurostat/cros/content/european-big-data-hackathon-2023_en) where open source software and tools are used 
 - Conferences
     - Eurostat biannualy organises the [NTTS conference](https://ec.europa.eu/eurostat/cros/content/NTTS2023_en) where open source and reproducibility is a topic
     - The yearly [use of R in official statistics (uRos) conference](https://www.r-project.ro/conference2022.html) promotes the use of opens source (R) for official statistics


